# Spring Lion + Thymeleaf Sample Application and Now Spring Security !

## What's this?
This is Lazar's (laz.tsarev@gmail.com) bachelor project that works with
 spring mvc, spring security and thymeleaf. The name of the project is
 lion and it is mentioned to help aisec members to handle their information 
about hosts and interns from the host program.

The git repo is : https://lazar_tsarev@bitbucket.org/lazar_tsarev/lion.git

## Running lion locally

First, you will have to download it. If you have a local installation of git, you can
do it by simply cloning this repository:

```
	git clone https://lazar_tsarev@bitbucket.org/lazar_tsarev/lion.git
```

If you don't have git installed, you can download a `.zip` by pressing on the 
*Download zip* button in the upper side of this page.

Once downloaded, the application can be locally started with:

```
	mvn tomcat7:run
```
The application is configured to use local MySql Database with name lion.
And login credentials for the DB user : root, pass : qwerty.
I suggest runing the application with the following command :
	mvn clean;mvn compile;mvn tomcat7:run;

Which will start a Tomcat server you can access at: http://localhost:8080/lion/
