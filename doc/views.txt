Views for hosts and interns : 
    1. Enter and edit description - for host
    2. Enter and edit description - for intern
    3. View for host/buddy/intern profile ( maybe some cool triangle showing the host/buddy/intern connection )
    4. Comments, recommendations, ratings, opinions about someone no matter if it is host/intern or even buddy

Views for aisec members:
    1. Search form for hosts
    2. Search form for interns         (or maybe 1 search form with criteria for if it has to be host/intern/aisec)
    3. Search form for aisec members
    4. Approval for new accounts
    5. Notes for hosts. ( and maybe for interns )
    6. Creating static profile for host/intern/buddy ?
    7. Approval for comments and ratings
    8. Approval for stories

General view for all :
    1. Sending personal messages
    2. Viewing personal messages
    3. Editing contact information
    4. Deactivating profile ?
    5. Forgotten password ( password recovery ).
    6. Password change
    7. Happy stories entering ( with approval from aisec member )
    8. Form for problems
    9. Choose what kind of notifications to receive
    10. Maybe follow someone, so when there is activity around this person you receive email
    11. Anonymous feedback ?
    12. Register
    13. Home page + login ( happy stories )

And finally SHOULD THERE BE CALENDAR WITH EVENTS ???
There should be calendar it is not that hard or better say I'm good enough to make one easily :)
