/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lion.dao;

import java.util.Collection;
import lion.model.CustomUser;
import lion.service.LionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 *
 * @author lazar
 */
@Component
public class CustomAuthenticationProvider implements UserDetailsService {

    @Qualifier("customUserRepository")
    private final LionService customUserService;

    @Autowired
    public CustomAuthenticationProvider(LionService customUserService) {
        this.customUserService = customUserService;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        CustomUser maUser = customUserService.getCustomUserByEmail(email);
        if (null == maUser) {
            throw new UsernameNotFoundException("User with email " + email + " is not found !");
        } else {
            return maUser;
        }
    }

}
