/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lion.examples;

import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletContext;
import lion.util.FileUploadForm;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author lazar
 */
@Controller
public class FileManagerController {

    @Autowired
    ServletContext servletContext;
    
    @RequestMapping(value = "examples/upload", method = RequestMethod.GET)
    public String uploadView(Model model) {
        model.addAttribute("fileUploadForm", new FileUploadForm());
        return "examples/upload";
    }

    @RequestMapping(value = "examples/upload", method = RequestMethod.POST)
    public String upload(@ModelAttribute("fileUploadForm") FileUploadForm fileUploadForm) {
        List<MultipartFile> uploadingFiles = fileUploadForm.getFiles();
        System.out.println("000000000000000000000000 -----> " + uploadingFiles.size());
        
        MultipartFile uploading = uploadingFiles.get(0);
        
        if (!uploading.isEmpty()) {
            try {
                validateImage(uploading);
            } catch (RuntimeException re) {
                throw re;
            }

            try {
                saveImage(System.currentTimeMillis() + ".jpg", uploading);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            System.out.println("----------------BEGIN----------------------");
            System.out.println(uploading.getContentType());
            System.out.println(uploading.getName());
            System.out.println(uploading.getOriginalFilename());
            System.out.println(uploading.getSize());
            System.out.println("-----------------END-----------------------");
        }
        return "redirect:/examples/viewAll";
    }

    private void validateImage(MultipartFile image) {
        if (!image.getContentType().equals("image/jpeg")) {
            throw new RuntimeException("Only JPG images are accepted");
        }
    }
    
    private void saveImage(String filename, MultipartFile image) throws RuntimeException, IOException {
        try {
            File file = new File(servletContext.getRealPath("/") + 
                                File.separator + "resources" + 
                                File.separator + "images" + 
                                File.separator + "uploaded" + 
                                File.separator + filename);

            FileUtils.writeByteArrayToFile(file, image.getBytes());
            System.out.println("Go to the location:  " + file.toString() + " on your computer and verify that the image has been stored.");
        } catch (IOException e) {
            throw e;
        }
    }
     
   @RequestMapping(value="examples/viewAll", method=RequestMethod.GET)
    public String viewAll() {
        return "examples/viewAll";
    }
}
