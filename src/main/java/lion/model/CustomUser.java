package lion.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table(name = "custom_users")
public class CustomUser extends BaseEntity implements UserDetails, Serializable {

    public enum UserType {
        INTERN,
        AIESEC,
        HOST;

        @Override
        public String toString() {
            String s = super.toString();
            return s.substring(0, 1) + s.substring(1).toLowerCase();
        }
        
        public static UserType getEnum(String value) {
            switch(value.toUpperCase()) {
                case "INTERN":
                    return INTERN;
                case "AIESEC":
                    return AIESEC;
                case "HOST":
                    return HOST;
            }
            throw new IllegalArgumentException("No such value in the enum : " + value + ".");
        }
    }

    @Column(name = "email", unique = true)
    @NotEmpty
    @NotNull
    private String email;

    @Column(name = "pass")
    @Size(min = 6, max = 60)
    private String pass;

    @Column(name = "name")
    @NotEmpty
    @NotNull
    private String name;

    @Column(name = "last_name")
    @NotEmpty
    @NotNull
    private String lastName;

    @Column(name = "activated")
    private boolean activated;

    @Column(name = "user_type")
    @Enumerated(EnumType.STRING)
    @NotNull
    private UserType userType;

    // TODO : consider orphanRemoval instead of manually remove the picture entity from the db
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "avatar_id", unique = true)
    private Picture avatar;

    @Column(name = "telephone")
    private String telephone;

    @Column(name = "skype")
    private String skype;

    @Column(name = "facebook")
    private String facebook;

    @Column(name = "interests")
    @Size(min = 10)
    private String interests;

    @Column(name = "birth_date")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    private DateTime birthDate;

    @Column(name = "english")
    @NotNull
    private String english;

    @Column(name = "is_static")
    private boolean isStatic;

    @Column(name = "gender")
    @NotNull
    private String gender;

    @Column(name = "questions")
    private String questions;

    @Transient
    Collection<? extends GrantedAuthority> authorities;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
        calcAuthorities();
    }

    public Picture getAvatar() {
        return avatar;
    }

    public void setAvatar(Picture avatar) {
        this.avatar = avatar;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getInterests() {
        return interests;
    }

    public void setInterests(String interests) {
        this.interests = interests;
    }

    public DateTime getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(DateTime birthDate) {
        this.birthDate = birthDate;
    }

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    public boolean isIsStatic() {
        return isStatic;
    }

    public void setIsStatic(boolean isStatic) {
        this.isStatic = isStatic;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getQuestions() {
        return questions;
    }

    public void setQuestions(String questions) {
        this.questions = questions;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (null == authorities) {
            calcAuthorities();
        }
        return authorities;
    }

    @Override
    public String getPassword() {
        return getPass();
    }

    @Override
    public String getUsername() {
        return getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return isActivated();
    }

    private void calcAuthorities() {
        switch (userType) {
            case INTERN:
                authorities = AuthorityUtils.createAuthorityList("ROLE_INTERN", "ROLE_USER");
                break;
            case AIESEC:
                authorities = AuthorityUtils.createAuthorityList("ROLE_AIESEC", "ROLE_USER");
                break;
            case HOST:
                authorities = AuthorityUtils.createAuthorityList("ROLE_HOST", "ROLE_USER");
                break;
            default:
                authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
                break;
        }
    }

    @Override
    public String toString() {
        return "CustomUser{" + "email=" + email + ", pass=" + pass + ", name=" + name + ", lastName=" + lastName + ", activated=" + activated + ", userType=" + userType + ", avatar=" + avatar + ", telephone=" + telephone + ", skype=" + skype + ", facebook=" + facebook + ", interests=" + interests + ", birthDate=" + birthDate + ", english=" + english + ", isStatic=" + isStatic + ", gender=" + gender + ", questions=" + questions + ", authorities=" + authorities + '}';
    }
}
