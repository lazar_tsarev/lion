package lion.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name = "feedbacks")
public class Feedback extends BaseEntity implements Serializable {

    @ManyToOne
    @JoinColumn(name = "author_id")
    @NotNull
    private CustomUser author;

    @ManyToOne
    @JoinColumn(name = "about_id")
    @NotNull
    private CustomUser about;

    @Column(name = "description")
    @NotNull
    @Size(min=10, max=10000)
    private String description;
    
    @Column(name = "date_created")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @NotNull
    private DateTime dateCreated;
    
    @Column(name = "approved")
    @NotNull
    private boolean approved;

    public CustomUser getAuthor() {
        return author;
    }

    public void setAuthor(CustomUser author) {
        this.author = author;
    }

    public CustomUser getAbout() {
        return about;
    }

    public void setAbout(CustomUser about) {
        this.about = about;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(DateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

}
