package lion.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "hosts")
public class Host extends BaseEntity implements Serializable {

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "user_id", unique = true)
    private CustomUser user;

    @Column(name = "max_interns")
    private int maxInterns;

    @Column(name = "smoking")
    private boolean smoking;

    @Column(name = "address")
    @NotEmpty
    private String address;

    @Column(name = "wheelchair_access")
    private boolean wheelchairAccess;

    @Column(name = "about_couch")
    private String aboutCouch;

    @Column(name = "private_room")
    private boolean privateRoom;

    @Column(name = "private_bed")
    private boolean privateBed;
    
    @Column(name = "starting_date")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    private DateTime startingDate;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "for_host_id")
    private List<Picture> pictures;
    
    public CustomUser getUser() {
        return user;
    }

    public void setUser(CustomUser user) {
        this.user = user;
    }

    public int getMaxInterns() {
        return maxInterns;
    }

    public void setMaxInterns(int maxInterns) {
        this.maxInterns = maxInterns;
    }

    public boolean isSmoking() {
        return smoking;
    }

    public void setSmoking(boolean smoking) {
        this.smoking = smoking;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isWheelchairAccess() {
        return wheelchairAccess;
    }

    public void setWheelchairAccess(boolean wheelchairAccess) {
        this.wheelchairAccess = wheelchairAccess;
    }

    public String getAboutCouch() {
        return aboutCouch;
    }

    public void setAboutCouch(String aboutCouch) {
        this.aboutCouch = aboutCouch;
    }

    public boolean isPrivateRoom() {
        return privateRoom;
    }

    public void setPrivateRoom(boolean privateRoom) {
        this.privateRoom = privateRoom;
    }

    public boolean isPrivateBed() {
        return privateBed;
    }

    public void setPrivateBed(boolean privateBed) {
        this.privateBed = privateBed;
    }

    public DateTime getStartingDate() {
        return startingDate;
    }

    public void setStartingDate(DateTime startingDate) {
        this.startingDate = startingDate;
    }

    public List<Picture> getPictures() {
        return pictures;
    }

    public void setPictures(List<Picture> pictures) {
        this.pictures = pictures.size() > 0 ? pictures : null;
    }

    @Override
    public String toString() {
        return "Host{" + "user=" + user + ", maxInterns=" + maxInterns + ", smoking=" + smoking + ", address=" + address + ", wheelchairAccess=" + wheelchairAccess + ", aboutCouch=" + aboutCouch + ", privateRoom=" + privateRoom + ", privateBed=" + privateBed + ", startingDate=" + startingDate + ", pictures=" + pictures + '}';
    }
}
