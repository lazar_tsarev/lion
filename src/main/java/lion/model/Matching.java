package lion.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "matchings")
public class Matching extends BaseEntity implements Serializable {
    // TODO : maybe matcher ??
    @ManyToOne
    @JoinColumn(name = "intern_id")
    @NotNull
    private CustomUser intern;

    @ManyToOne
    @JoinColumn(name = "host_id")
    @NotNull
    private CustomUser host;

    @ManyToOne
    @JoinColumn(name = "buddy_id")
    private CustomUser buddy;

    @Column(name = "start_date")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    private DateTime startDate;

    @Column(name = "end_date")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    private DateTime endDate;

    public CustomUser getIntern() {
        return intern;
    }

    public void setIntern(CustomUser intern) {
        this.intern = intern;
    }

    public CustomUser getHost() {
        return host;
    }

    public void setHost(CustomUser host) {
        this.host = host;
    }

    public CustomUser getBuddy() {
        return buddy;
    }

    public void setBuddy(CustomUser buddy) {
        this.buddy = buddy;
    }

    public DateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(DateTime startDate) {
        this.startDate = startDate;
    }

    public DateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(DateTime endDate) {
        this.endDate = endDate;
    }

}
