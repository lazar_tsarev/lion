package lion.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "messages")
public class Message extends BaseEntity implements Serializable {
    
    @ManyToOne
    @JoinColumn(name = "sender_id")
    @NotNull
    private CustomUser sender;

    @ManyToOne
    @JoinColumn(name = "receiver_id")
    @NotNull
    private CustomUser receiver;

    @Column(name = "content")
    @NotEmpty
    @NotNull
    @Size(max=1000)
    private String content;
    
    @Column(name = "title")
    @Size(max=250)
    @NotNull
    @NotEmpty
    private String title;

    @Column(name = "date_created")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @NotNull
    private DateTime dateCreated;
    
    @Column(name="readed")
    private boolean readed;

    public CustomUser getSender() {
        return sender;
    }

    public void setSender(CustomUser sender) {
        this.sender = sender;
    }

    public CustomUser getReceiver() {
        return receiver;
    }

    public void setReceiver(CustomUser receiver) {
        this.receiver = receiver;
    }

    public String getMessage() {
        return content;
    }

    public void setMessage(String message) {
        this.content = message;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public DateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(DateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public boolean isReaded() {
        return readed;
    }

    public void setReaded(boolean readed) {
        this.readed = readed;
    }
}
