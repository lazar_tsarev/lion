package lion.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "notes")
public class Note extends BaseEntity implements Serializable {

    // the author is ALWAYS an AIESEC member
    @ManyToOne
    @JoinColumn(name = "author_id")
    private CustomUser author;

    @ManyToOne
    @JoinColumn(name = "about_id")
    private CustomUser about;

    @Column(name = "description")
    @NotEmpty
    private String description;

    @Column(name = "date_created")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    private DateTime dateCreated;

    public CustomUser getAuthor() {
        return author;
    }

    public void setAuthor(CustomUser author) {
        this.author = author;
    }

    public CustomUser getAbout() {
        return about;
    }

    public void setAbout(CustomUser about) {
        this.about = about;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(DateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

}
