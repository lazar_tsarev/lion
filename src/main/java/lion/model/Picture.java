package lion.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "pictures")
public class Picture extends BaseEntity implements Serializable {
	
    public Picture() {
    }
	
    public Picture(String location, String alt) {
        this.location = location;
        this.alt = alt;
    }
    
	@Column(name = "location")
	@NotEmpty
	private String location;

	@Column(name = "alt")
	private String alt;
	
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getAlt() {
		return alt;
	}

	public void setAlt(String alt) {
		this.alt = alt;
	}
}
