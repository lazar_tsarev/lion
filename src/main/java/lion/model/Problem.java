package lion.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "problems")
public class Problem extends BaseEntity implements Serializable {

    @ManyToOne
    @JoinColumn(name = "author_id")
    private CustomUser author;

    @ManyToOne
    @JoinColumn(name = "about_id")
    private CustomUser about;

    @Column(name = "description")
    @NotEmpty
    @Size(min=20, max=5000)
    private String description;

    public CustomUser getAuthor() {
        return author;
    }

    public void setAuthor(CustomUser author) {
        this.author = author;
    }

    public CustomUser getAbout() {
        return about;
    }

    public void setAbout(CustomUser about) {
        this.about = about;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
