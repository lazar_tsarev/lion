package lion.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "stories")
public class Story extends BaseEntity implements Serializable {

    @ManyToOne
    @JoinColumn(name = "author_id")
    private CustomUser author;

    @ManyToOne
    @JoinColumn(name = "about_id")
    private CustomUser about;

    @Column(name = "started_date")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    private DateTime startedDate;

    @Column(name = "ended_date")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    private DateTime endedDate;

    @Column(name = "description")
    @NotEmpty
    private String description;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "for_story_id")
    private List<Picture> pictures;

    @Column(name = "video_link")
    private String videoLink;
    
    @Column(name = "approved")
    private boolean approved;

    public CustomUser getAuthor() {
        return author;
    }

    public void setAuthor(CustomUser author) {
        this.author = author;
    }

    public CustomUser getAbout() {
        return about;
    }

    public void setAbout(CustomUser about) {
        this.about = about;
    }

    public DateTime getStartedDate() {
        return startedDate;
    }

    public void setStartedDate(DateTime startedDate) {
        this.startedDate = startedDate;
    }

    public DateTime getEndedDate() {
        return endedDate;
    }

    public void setEndedDate(DateTime endedDate) {
        this.endedDate = endedDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Picture> getPictures() {
        return pictures;
    }

    public void setPictures(List<Picture> pictures) {
        this.pictures = pictures;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }

    public boolean getApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

}
