/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lion.repository;

import java.util.List;
import lion.model.CustomUser;
import lion.model.CustomUser.UserType;
import lion.util.SearchForm;

/**
 *
 * @author lazar
 */
public interface CustomUserRepository {
    CustomUser getCustomUserByEmail(String email);
    CustomUser getCustomUserByID(int id);
    CustomUser createCustomUser(CustomUser user);
    CustomUser updateCustomUser(CustomUser user);
    List<CustomUser> getCustomUsersWithType(UserType type);
    List<CustomUser> getAllUsersWithout(int userId);
    List<CustomUser> getNewUsers();
    List<CustomUser> filterUsers(SearchForm searchForm);
}
