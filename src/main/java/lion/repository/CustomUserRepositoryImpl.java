/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lion.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import lion.model.CustomUser;
import lion.model.CustomUser.UserType;
import lion.util.SearchForm;
import lion.util.UserUtil;
import org.joda.time.DateTime;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lazar
 */
@Repository
public class CustomUserRepositoryImpl implements CustomUserRepository {

    @PersistenceContext
    private EntityManager em;
    
    @Override
    public CustomUser getCustomUserByEmail(String email) {
        Query query = this.em.createQuery("SELECT customUser FROM CustomUser customUser WHERE customUser.email =:email");
        query.setParameter("email", email);
        List<CustomUser> result = (List<CustomUser>) query.getResultList();
        return result.size() == 1 ? result.get(0) : null;
    }
    
    @Override
    public CustomUser getCustomUserByID(int id) {
        Query query = this.em.createQuery("SELECT customUser FROM CustomUser customUser WHERE customUser.id =:id");
        query.setParameter("id", id);
        List<CustomUser> result = (List<CustomUser>) query.getResultList();
        return result.size() == 1 ? result.get(0) : null;
    }
    
    @Override
    public CustomUser createCustomUser(CustomUser user) {
        user.setPass(UserUtil.hashPass(user.getPass()));
        return (CustomUser) em.merge(user);
    }
    
    @Override
    public CustomUser updateCustomUser(CustomUser user) {
        return (CustomUser) em.merge(user);
    }
    
    @Override
    public List<CustomUser> getCustomUsersWithType(UserType type) {
        Query query = this.em.createQuery("SELECT customUser FROM CustomUser customUser "
                                        + "WHERE customUser.userType = :userType AND activated = :activated");
        query.setParameter("userType", type);
        query.setParameter("activated", true);
        return query.getResultList();
    }
    
    @Override
    public List<CustomUser> getAllUsersWithout(int userId) {
        Query query = this.em.createQuery("SELECT customUser FROM CustomUser customUser "
                                        + "WHERE customUser.id != :userId AND activated = :activated");
        query.setParameter("userId", userId);
        query.setParameter("activated", true);
        List<CustomUser> result = (List<CustomUser>) query.getResultList();
        return result;
    }
    
    @Override
    public List<CustomUser> getNewUsers() {
        Query query = this.em.createQuery("SELECT customUser FROM CustomUser customUser WHERE customUser.activated = :activated");
        query.setParameter("activated", false);
        List<CustomUser> result = (List<CustomUser>) query.getResultList();
        return result;
    }
    
    @Override
    public List<CustomUser> filterUsers(SearchForm searchForm) {
        Map<String, Object> params = new HashMap<>();
        StringBuilder queryBuilder = new StringBuilder("SELECT customUser FROM CustomUser customUser WHERE activated = :activated AND ");
        params.put("activated", true);
        String  email = searchForm.getEmail(),
                gender = searchForm.getGender(),
                lastName = searchForm.getLastName(),
                name = searchForm.getName(),
                type = searchForm.getType();
        DateTime bornAfter = searchForm.getBornAfter(),
                bornBefore = searchForm.getBornBefore();
        
        if(!email.isEmpty()) {
            queryBuilder.append(" customUser.email LIKE :email AND ");
            params.put("email", "%" + email + "%");
        }
        
        if(!lastName.isEmpty()) {
            queryBuilder.append(" customUser.lastName LIKE :lastName AND ");
            params.put("lastName", "%" + lastName + "%");
        }
        
        if(!name.isEmpty()) {
            queryBuilder.append(" customUser.name LIKE :name AND ");
            params.put("name", "%" + name + "%");
        }
        
        if(!gender.isEmpty() && !gender.equals(SearchForm.ANY)) {
            queryBuilder.append(" customUser.gender = :gender AND ");
            params.put("gender", gender.toLowerCase());
        }
        
        if(!type.isEmpty() && !type.equals(SearchForm.ANY)) {
            queryBuilder.append(" customUser.userType = :type AND ");
            params.put("type", UserType.getEnum(type));
        }
        
        if(bornAfter != null) {
            queryBuilder.append(" customUser.birthDate > :bornAfter AND ");
            params.put("bornAfter", bornAfter);
        }
        
        if(bornBefore != null) {
            queryBuilder.append(" customUser.birthDate > :bornBefore AND ");
            params.put("bornBefore", bornBefore);
        }
        
        int size = queryBuilder.length();
        queryBuilder.delete(size - 4, size);
        
        Query query = this.em.createQuery(queryBuilder.toString());
        for(Entry<String, Object> param : params.entrySet()) {
            query.setParameter(param.getKey(), param.getValue());
        }
        
        List<CustomUser> result = (List<CustomUser>) query.getResultList();
        return result;
    }
}
