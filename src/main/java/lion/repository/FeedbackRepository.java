/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lion.repository;

import java.util.List;
import lion.model.Feedback;
import lion.model.Note;

/**
 *
 * @author lazar
 */
public interface FeedbackRepository {
    public Feedback createNewFeedback(Feedback feedback);
    public Note createNewNote(Note newNote);
    public List<Note> getNotesAboutUser(int aboutUserId);
}
