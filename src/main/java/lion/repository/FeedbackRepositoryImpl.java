/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lion.repository;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import lion.model.Feedback;
import lion.model.Note;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lazar
 */
@Repository
public class FeedbackRepositoryImpl implements FeedbackRepository {

    @PersistenceContext
    private EntityManager em;
    
    @Override
    public Feedback createNewFeedback(Feedback feedback) {
        return (Feedback) em.merge(feedback);
    }
    
    @Override
    public Note createNewNote(Note newNote) {
        return (Note) em.merge(newNote);
    }
    
    @Override
    public List<Note> getNotesAboutUser(int aboutUserId) {
        Query query = this.em.createQuery("SELECT note FROM Note note WHERE note.about.id = :aboutUserId");
        query.setParameter("aboutUserId", aboutUserId);
        return (List<Note>) query.getResultList();
    }
}
