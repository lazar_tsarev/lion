/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lion.repository;

import lion.model.Host;

/**
 *
 * @author lazar
 */
public interface HostRepository {
    Host createHost(Host host);
    Host updateHost(Host host);
    Host getHostForCustomUser(int userId);
}
