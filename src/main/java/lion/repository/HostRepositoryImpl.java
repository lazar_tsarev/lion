/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lion.repository;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import lion.model.CustomUser;
import lion.model.Host;
import lion.util.UserUtil;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lazar
 */
@Repository
public class HostRepositoryImpl implements HostRepository {

    @PersistenceContext
    private EntityManager em;
    
    @Override
    public Host createHost(Host host) {
        CustomUser user = host.getUser();
        user.setPass(UserUtil.hashPass(user.getPass()));
        return (Host) em.merge(host);
    }
    
    @Override
    public Host updateHost(Host host) {
        System.out.println(host.toString());
        return (Host) em.merge(host);
    }
    
    @Override
    public Host getHostForCustomUser(int userId) {
        Query query = this.em.createQuery("SELECT host FROM Host host WHERE host.user.id = :userId");
        query.setParameter("userId", userId);
        List<Host> result = query.getResultList();
        return result.size() == 1 ? result.get(0) : null;
    }
    
}
