/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lion.repository;

import java.util.List;
import lion.model.CustomUser;
import lion.model.Intern;

/**
 *
 * @author lazar
 */
public interface InternRepository {
    Intern createIntern(Intern intern);
    Intern getInternForCustomUser(int userId);
    Intern updateIntern(Intern intern);
    List<CustomUser> getHostsMatchedWith(int userId);
}
