/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lion.repository;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import lion.model.CustomUser;
import lion.model.Intern;
import lion.util.UserUtil;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lazar
 */
@Repository
public class InternRepositoryImpl implements InternRepository {

    @PersistenceContext
    private EntityManager em;
    
    @Override
    public Intern createIntern(Intern intern) {
        CustomUser user = intern.getUser();
        user.setPass(UserUtil.hashPass(user.getPass()));
        return (Intern) em.merge(intern);
    }
    
    @Override
    public Intern getInternForCustomUser(int userId) {
        Query query = this.em.createQuery("SELECT intern FROM Intern intern WHERE intern.user.id = :userId");
        query.setParameter("userId", userId);
        List<Intern> result = (List<Intern>) query.getResultList();
        return result.size() == 1 ? result.get(0) : null;

    }
    
    @Override
    public Intern updateIntern(Intern intern) {
        return (Intern) em.merge(intern);
    }
    
    @Override
    public List<CustomUser> getHostsMatchedWith(int userId) {
        Query query = this.em.createQuery("SELECT host FROM Host host WHERE host.user.id IN "
                                        + "(SELECT matching.host.id FROM Matching matching WHERE matching.intern.id = :internId)");
        query.setParameter("internId", userId);
        return (List<CustomUser>) query.getResultList();
    }
}
