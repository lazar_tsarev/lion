/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lion.repository;

import java.util.List;
import lion.model.Matching;

/**
 *
 * @author lazar
 */
public interface MatchingRepository {
    public Matching createNewMatching(Matching matching);
    public List<Matching> getMatchingsWithUniqueHost(int userId);
    public List<Matching> getMatchingsWithUniqueIntern(int userId);
    public boolean hasMatchingWithUsers(int userId, int viewUserId);
}
