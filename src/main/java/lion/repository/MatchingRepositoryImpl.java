/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lion.repository;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import lion.model.Matching;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lazar
 */
@Repository
public class MatchingRepositoryImpl implements MatchingRepository {

    @PersistenceContext
    private EntityManager em;
    
    @Override
    public Matching createNewMatching(Matching matching) {
        return (Matching) em.merge(matching);
    }
    
    @Override
    public List<Matching> getMatchingsWithUniqueHost(int userId) {
        Query query = this.em.createQuery("SELECT matching FROM Matching matching WHERE matching.intern.id = :internId ORDER BY matching.startDate");
        query.setParameter("internId", userId);
        
        List<Matching> allMatchings = (List<Matching>) query.getResultList();
        List<Integer> hosts = new ArrayList<>();
        List<Matching> matchings = new ArrayList<>();
        for(Matching matching : allMatchings) {
            int hostId = matching.getHost().getId();
            if(!hosts.contains(hostId)) {
                matchings.add(matching);
                hosts.add(hostId);
            }
        }
        
        return matchings;
    }
    
    @Override
    public List<Matching> getMatchingsWithUniqueIntern(int userId) {
        Query query = this.em.createQuery("SELECT matching FROM Matching matching WHERE matching.host.id = :hostId ORDER BY matching.startDate");
        query.setParameter("hostId", userId);
        
        List<Matching> allMatchings = (List<Matching>) query.getResultList();
        List<Integer> interns = new ArrayList<>();
        List<Matching> matchings = new ArrayList<>();
        for(Matching matching : allMatchings) {
            int internId = matching.getHost().getId();
            if(!interns.contains(internId)) {
                matchings.add(matching);
                interns.add(internId);
            }
        }
        
        return matchings;
    }
    
    @Override
    public boolean hasMatchingWithUsers(int userId, int viewUserId) {
        Query query = this.em.createQuery("SELECT matching FROM Matching matching WHERE "
                                        + "(matching.intern.id = :userId OR matching.host.id = :userId OR matching.buddy.id = :userId) AND"
                                        + "(matching.intern.id = :viewUserId OR matching.host.id = :viewUserId OR matching.buddy.id = :viewUserId)");
        
        query.setParameter("userId", userId);
        query.setParameter("viewUserId", viewUserId);
        
        return query.getResultList().size() > 0;
    }
}
