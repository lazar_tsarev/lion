/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lion.repository;

import java.util.List;
import lion.model.Message;

/**
 *
 * @author lazar
 */
public interface MessagesRepository {
    public List<Message> getMessagesForUsers(int userId, int otherUserId);
    public List<Message> getConversationsForUser(int userId);
    public List<Message> getSentConversationsForUser(int userId);
    public Message createNewMessage(Message message);
}
