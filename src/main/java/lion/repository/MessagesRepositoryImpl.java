/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lion.repository;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import lion.model.Message;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lazar
 */
@Repository
public class MessagesRepositoryImpl implements MessagesRepository {

    @PersistenceContext
    private EntityManager em;
    
    @Override
    public List<Message> getMessagesForUsers(int userId, int otherUserId) {
        Query query = this.em.createQuery("SELECT message FROM Message message WHERE "
                                        + "(message.receiver.id =:receiver AND message.sender.id = :sender) OR "
                                        + "(message.sender.id =:receiver AND message.receiver.id = :sender) "
                                        + " ORDER BY message.dateCreated DESC");
        query.setParameter("receiver", userId);
        query.setParameter("sender", otherUserId);
        List<Message> result = (List<Message>) query.getResultList();
        
        for(Message message : result) {
            int receiverId = message.getReceiver().getId();
            if(!message.isReaded() && receiverId == userId) {
                message.setReaded(true);
                em.merge(message);
            }
        }
        
        return result;
    }
    
    @Override
    public List<Message> getConversationsForUser(int userId) {
        Query query = this.em.createQuery("SELECT message FROM Message message WHERE message.receiver.id = :receiver ORDER BY message.dateCreated DESC");
        query.setParameter("receiver", userId);
        List<Message> allMessages = (List<Message>) query.getResultList();
        
        List<Message> conversations = new ArrayList<>();
        List<Integer> senders = new ArrayList<>();
        
        for(Message message : allMessages) {
            int senderId = message.getSender().getId();
            if(!senders.contains(senderId)) {
                conversations.add(message);
                senders.add(senderId);
            }
        }
        return conversations;
    }
    
    @Override
    public List<Message> getSentConversationsForUser(int userId) {
        Query query = this.em.createQuery("SELECT message FROM Message message WHERE message.sender.id = :sender ORDER BY message.dateCreated DESC");
        query.setParameter("sender", userId);
        List<Message> allMessages = (List<Message>) query.getResultList();
        
        List<Message> conversations = new ArrayList<>();
        List<Integer> receivers = new ArrayList<>();
        
        for(Message message : allMessages) {
            int senderId = message.getSender().getId();
            if(!receivers.contains(senderId)) {
                conversations.add(message);
                receivers.add(senderId);
            }
        }
        return conversations;
    }
    
    @Override
    public Message createNewMessage(Message message) {
        return (Message) em.merge(message);
    }
}
