/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lion.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import lion.model.Picture;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lazar
 */
@Repository
public class PictureRepositoryImpl implements PictureRepository {

    @PersistenceContext
    private EntityManager em;
    
    @Override
    public void deletePicture(Picture picture) {
        em.remove(em.merge(picture));
    }
    
}
