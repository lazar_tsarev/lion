/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lion.repository;

import java.util.List;
import lion.model.Problem;

/**
 *
 * @author lazar
 */
public interface ProblemRepository {
    Problem createProblem(Problem problem);
    List<Problem> getProblems();
}
