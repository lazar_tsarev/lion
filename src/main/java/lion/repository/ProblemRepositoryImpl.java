/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lion.repository;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import lion.model.Problem;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lazar
 */
@Repository
public class ProblemRepositoryImpl implements ProblemRepository {

    @PersistenceContext
    private EntityManager em;
    
    @Override
    public Problem createProblem(Problem problem) {
        return (Problem) em.merge(problem);
    }
    
    @Override
    public List<Problem> getProblems() {
        Query query = em.createQuery("SELECT problem FROM Problem problem");
        return query.getResultList();
    }
    
}
