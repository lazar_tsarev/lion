/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lion.repository;

import java.util.List;
import lion.model.Story;

/**
 *
 * @author lazar
 */
public interface StoryRepository {
    Story createStory(Story story);
    Story updateStory(Story story);
    Story getStory(int storyId);
    List<Story> getNewStories();
}
