/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lion.repository;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import lion.model.Story;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lazar
 */
@Repository
public class StoryRepositoryImpl implements StoryRepository {

    @PersistenceContext
    private EntityManager em;
    
    @Override
    public Story createStory(Story story) {
        return (Story) em.merge(story);
    }
    
    @Override
    public Story updateStory(Story story) {
        return (Story) em.merge(story);
    }
    
    @Override
    public Story getStory(int storyId) {
        Query query = em.createQuery("SELECT story FROM Story story WHERE story.id = :storyId");
        query.setParameter("storyId", storyId);
        List<Story> result = query.getResultList();
        return result.size() == 1 ? result.get(0) : null;
    }
    
    @Override
    public List<Story> getNewStories() {
        Query query = em.createQuery("SELECT story FROM Story story WHERE story.approved = :approved");
        query.setParameter("approved", false);
        return query.getResultList();
    }
    
}
