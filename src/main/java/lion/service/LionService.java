/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lion.service;

import java.util.List;
import lion.model.CustomUser;
import lion.model.CustomUser.UserType;
import lion.model.Feedback;
import lion.model.Host;
import lion.model.Intern;
import lion.model.Matching;
import lion.model.Message;
import lion.model.Note;
import lion.model.Picture;
import lion.model.Problem;
import lion.model.Story;
import lion.util.SearchForm;

/**
 *
 * @author lazar
 */
public interface LionService {
    public CustomUser getCustomUserByEmail(String email);
    public CustomUser getCustomUserByID(int id);
    public CustomUser createCustomUser(CustomUser user);
    public CustomUser updateCustomUser(CustomUser user);
    public List<CustomUser> getCustomUsersWithType(UserType type);
    public List<CustomUser> getNewUsers();
    public List<CustomUser> filterUsers(SearchForm searchForm);
    
    public Host createHost(Host host);
    public Host updateHost(Host host);
    public Host getHostForCustomUser(int userId);
    
    public Intern createIntern(Intern intern);
    public Intern getInternForCustomUser(int userId);
    public Intern updateIntern(Intern intern);
    
    public Problem createProblem(Problem problem);
    public List<Problem> getProblems();
    
    public Story createStory(Story story);
    public Story updateStory(Story story);
    public Story getStory(int storyId);
    public List<Story> getNewStories();
    
    public void deletePicture(Picture picture);
    
    public List<Message> getMessagesForUsers(int userId, int otherUserId);
    public List<Message> getConversationsForUser(int userId);
    public List<Message> getSentConversationsForUser(int userId);
    public Message createNewMessage(Message message);
    
    public List<CustomUser> getAllUsersWithout(int userId);
    
    public Matching createNewMatching(Matching matching);
    public List<Matching> getMatchingsWithUniqueHost(int userId);
    public List<Matching> getMatchingsWithUniqueIntern(int userId);
    public boolean hasMatchingWithUsers(int userId, int viewUserId);
    
    public Feedback createNewFeedback(Feedback feedback);
    public Note createNewNote(Note newNote);
    public List<Note> getNotesAboutUser(int aboutUserId);
}
