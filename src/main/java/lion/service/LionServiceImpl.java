/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lion.service;

import java.util.List;
import lion.model.CustomUser;
import lion.model.CustomUser.UserType;
import lion.model.Feedback;
import lion.model.Host;
import lion.model.Intern;
import lion.model.Matching;
import lion.model.Message;
import lion.model.Note;
import lion.model.Picture;
import lion.model.Problem;
import lion.model.Story;
import lion.repository.CustomUserRepository;
import lion.repository.FeedbackRepository;
import lion.repository.HostRepository;
import lion.repository.InternRepository;
import lion.repository.MatchingRepository;
import lion.repository.MessagesRepository;
import lion.repository.PictureRepository;
import lion.repository.ProblemRepository;
import lion.repository.StoryRepository;
import lion.util.SearchForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author lazar
 */
@Service
public class LionServiceImpl implements LionService {

    CustomUserRepository customUserRepo;
    HostRepository hostRepo;
    InternRepository internRepo;
    ProblemRepository problemRepo;
    StoryRepository storyRepo;
    PictureRepository pictureRepo;
    MessagesRepository messagesRepo;
    MatchingRepository matchingRepo;
    FeedbackRepository feedbackRepo;
    
    @Autowired
    public LionServiceImpl( CustomUserRepository customUserRepo, HostRepository hostRepo, InternRepository internRepo, 
                            ProblemRepository problemRepo, StoryRepository storyRepo, MessagesRepository messagesRepo, 
                            PictureRepository pictureRepo, MatchingRepository matchingRepo, FeedbackRepository feedbackRepo) {
        this.customUserRepo = customUserRepo;
        this.hostRepo = hostRepo;
        this.internRepo = internRepo;
        this.problemRepo = problemRepo;
        this.storyRepo = storyRepo;
        this.pictureRepo = pictureRepo;
        this.messagesRepo = messagesRepo;
        this.matchingRepo = matchingRepo;
        this.feedbackRepo = feedbackRepo;
    }
    
    @Transactional
    @Override
    public CustomUser getCustomUserByEmail(String email) {
        return customUserRepo.getCustomUserByEmail(email);
    }
    
    @Transactional
    @Override
    public CustomUser getCustomUserByID(int id) {
        return customUserRepo.getCustomUserByID(id);
    }
    
    @Transactional
    @Override
    public CustomUser createCustomUser(CustomUser user) {
        return customUserRepo.createCustomUser(user);
    }
    
    @Transactional
    @Override
    public CustomUser updateCustomUser(CustomUser user) {
        return customUserRepo.updateCustomUser(user);
    }
    
    @Transactional
    @Override
    public List<CustomUser> getCustomUsersWithType(UserType type) {
        return customUserRepo.getCustomUsersWithType(type);
    }
    
    @Transactional
    @Override
    public List<CustomUser> getNewUsers() {
        return customUserRepo.getNewUsers();
    }
    
    @Transactional
    @Override
    public List<CustomUser> filterUsers(SearchForm searchForm) {
        return customUserRepo.filterUsers(searchForm);
    }
    
    @Transactional
    @Override
    public void deletePicture(Picture picture) {
        pictureRepo.deletePicture(picture);
    }
    
    @Transactional
    @Override
    public List<CustomUser> getAllUsersWithout(int userId) {
        return customUserRepo.getAllUsersWithout(userId);
    }
    
    @Transactional
    @Override
    public Host createHost(Host host) {
        return hostRepo.createHost(host);
    }
    
    @Transactional
    @Override
    public Host updateHost(Host host) {
        return hostRepo.updateHost(host);
    }
    
    @Transactional
    @Override
    public Host getHostForCustomUser(int userId) {
        return hostRepo.getHostForCustomUser(userId);
    }
    
    @Transactional
    @Override
    public Intern createIntern(Intern intern) {
        return internRepo.createIntern(intern);
    }
    
    @Transactional
    @Override
    public Problem createProblem(Problem problem) {
        return problemRepo.createProblem(problem);
    }
    
    @Transactional
    @Override
    public List<Problem> getProblems() {
        return problemRepo.getProblems();
    }
    
    @Transactional
    @Override
    public Story createStory(Story story) {
        return storyRepo.createStory(story);
    }
    
    @Transactional
    @Override
    public Story updateStory(Story story) {
        return storyRepo.updateStory(story);
    }
    
    @Transactional
    @Override
    public Story getStory(int storyId) {
        return storyRepo.getStory(storyId);
    }
    
    @Transactional
    @Override
    public List<Story> getNewStories() {
        return storyRepo.getNewStories();
    }
    
    @Transactional
    @Override
    public List<Message> getMessagesForUsers(int userId, int otherUserId) {
        return messagesRepo.getMessagesForUsers(userId, otherUserId);
    }
    
    @Transactional
    @Override
    public List<Message> getConversationsForUser(int userId) {
        return messagesRepo.getConversationsForUser(userId);
    }
    
    @Transactional
    @Override
    public List<Message> getSentConversationsForUser(int userId) {
        return messagesRepo.getSentConversationsForUser(userId);
    }
    
    @Transactional
    @Override
    public Message createNewMessage(Message message) {
        return messagesRepo.createNewMessage(message);
    }
    
    @Transactional
    @Override
    public Intern getInternForCustomUser(int userId) {
        return internRepo.getInternForCustomUser(userId);
    }
    
    @Transactional
    @Override
    public Intern updateIntern(Intern intern) {
        return internRepo.updateIntern(intern);
    }
    
    @Transactional
    @Override
    public Matching createNewMatching(Matching matching) {
        return matchingRepo.createNewMatching(matching);
    }
    
    @Transactional
    @Override
    public List<Matching> getMatchingsWithUniqueHost(int userId) {
        return matchingRepo.getMatchingsWithUniqueHost(userId);
    }
    
    @Transactional
    @Override
    public List<Matching> getMatchingsWithUniqueIntern(int userId) {
        return matchingRepo.getMatchingsWithUniqueIntern(userId);
    }
    
    @Transactional
    @Override
    public boolean hasMatchingWithUsers(int userId, int viewUserId) {
        return matchingRepo.hasMatchingWithUsers(userId, viewUserId);
    }
    
    @Transactional
    @Override
    public Feedback createNewFeedback(Feedback feedback) {
        return feedbackRepo.createNewFeedback(feedback);
    }
    
    @Transactional
    @Override
    public Note createNewNote(Note newNote) {
        return feedbackRepo.createNewNote(newNote);
    }
    
    @Transactional
    @Override
    public List<Note> getNotesAboutUser(int aboutUserId) {
        return feedbackRepo.getNotesAboutUser(aboutUserId);
    }
}
