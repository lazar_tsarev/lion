/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lion.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author lazar
 */
public final class FileUploadUtil {
    
    public static String saveImage(String filename, String type, BufferedImage image, ServletContext servletContext) throws RuntimeException, IOException {
        String relPath =    File.separator + "resources" + 
                            File.separator + "images" + 
                            File.separator + "uploaded" + 
                            File.separator + filename;
        ImageIO.write(image, type, new File(servletContext.getRealPath("/") + relPath));
        return relPath;
    }
    
    public static String saveImage(MultipartFile file, ServletContext servletContext) throws RuntimeException, IOException {
        String type = file.getContentType().equals("image/jpeg") ? "jpg" : "png";
        BufferedImage bufferedImage = ImageIO.read(file.getInputStream());
        String fileName = UserUtil.hashPass(Long.toString(System.currentTimeMillis())).substring(10);
        fileName = fileName.replace('/', 'a'); // escapes linux path separator
        return saveImage(fileName + "." + type, type, bufferedImage, servletContext);
    }
}
