package lion.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.joda.time.DateTime;
import org.springframework.validation.BindingResult;

/**
 *
 * @author lazar
 */
public class SearchForm {
    
    public static final String ANY = "any";
    
    private DateTime bornAfter, bornBefore;
    private String email, name, lastName, gender, type;
    private static final List<String> genders, types;
    
    static {
        genders = new ArrayList<>(Arrays.asList("any", "male", "female"));
        types = new ArrayList<>(Arrays.asList("HOST", "AIESEC", "INTERN"));
    }
    
    public SearchForm() {
        email = "";
        name = "";
        lastName = "";
        gender = ANY;
        type = ANY;
    }
    
    public void validate(BindingResult result) {
        if(bornAfter != null && bornBefore != null && bornAfter.isAfter(bornBefore)) {
            result.rejectValue("bornAfter", "after.must.be.before.born");
        }
        
        if(!gender.isEmpty() && !genders.contains(gender.toLowerCase())) {
            result.rejectValue("gender", "invalid.gender");
        }
        
        if(!type.isEmpty() && !types.contains(type.toUpperCase())) {
            result.rejectValue("type", "invalid.type");
        }
    }

    public DateTime getBornAfter() {
        return bornAfter;
    }

    public void setBornAfter(DateTime bornAfter) {
        this.bornAfter = bornAfter;
    }

    public DateTime getBornBefore() {
        return bornBefore;
    }

    public void setBornBefore(DateTime bornBefore) {
        this.bornBefore = bornBefore;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
}
