package lion.util;

import java.util.Properties;
import javassist.bytecode.Bytecode;

import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendMail {

    private final String from;
    private final String to;
    private final String subject;
    private final String text;
    private final String userPass;

    private final String smtpHost;
    private final String smtpPort;
    private final String type;
    
    private static final String myEmail = "aiesec.lion.mail@gmail.com",
                                myPass = "stra6naparola",
                                mySMTPHost = "smtp.gmail.com",
                                mySMTPPort = "587",
                                myType = "TLS";

    private SendMail(String from, String to, String subject, String text, String userPass, String smtpHost, String smtpPort, String type) {
        this.from = from;
        this.to = to;
        this.subject = subject;
        this.text = text;
        this.userPass = userPass;
        this.smtpHost = smtpHost;
        this.smtpPort = smtpPort;
        this.type = type;
    }
    
    public static boolean sendEmail(String from, String to, String subject, String text, String userPass, String smtpHost, String smtpPort, String type) {
        return new SendMail(from, to, subject, text, userPass, smtpHost, smtpPort, type).send();
    }
    
    public static boolean sendEmail(String to, String subject, String text) {
        return sendEmail(myEmail, to, subject, text, myPass, mySMTPHost, mySMTPPort, myType);
    }

    private boolean send() {
        boolean isSuccessful = false;
        Properties props = new Properties();
        
        if (type.equalsIgnoreCase("TLS")) {
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", smtpHost);
            props.put("mail.smtp.port", smtpPort);
        } else if (type.equalsIgnoreCase("SSL")) {
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.socketFactory.port", smtpPort);
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.host", smtpHost);
            props.put("mail.smtp.port", smtpPort);
        }

        Session mailSession = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, userPass);
            }
        });
        
        try {
            Message message = new MimeMessage(mailSession);
            InternetAddress fromAddress = new InternetAddress(from),
                            toAddress = new InternetAddress(to);
            
            message.setFrom(fromAddress);
            message.setRecipient(RecipientType.TO, toAddress);
            message.setSubject(subject);
            message.setText(text);

            Transport.send(message);
            
            isSuccessful = true;
            System.out.println("Email sent successfully");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return isSuccessful;
    }
}
