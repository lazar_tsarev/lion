package lion.util;

import lion.model.CustomUser;
import lion.service.LionService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 *
 * @author lazar
 */
public final class UserUtil {
    public static CustomUser getCustomUserFromSession(LionService lionService) {
        String email = SecurityContextHolder.getContext().getAuthentication().getName();
        return lionService.getCustomUserByEmail(email);
    }
    
    public static String hashPass(String purePass) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(purePass);
    }
    
    public static boolean passMatch(String purePass, String hashedPassword) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.matches(purePass, hashedPassword);
    }
}
