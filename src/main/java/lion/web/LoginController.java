package lion.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author lazar
 */
@Controller
public class LoginController {
    @RequestMapping(value = {"/", "/login"})
    public String login() {
        return "login";
    }
    
    @RequestMapping("/login-error")
    public String loginError(Model model) {
        model.addAttribute("loginError", true);
        return "login";
    }
    
    @RequestMapping("/register")
    public String register() {
        return "/register";
    }
    
    @RequestMapping("/403")
    public String accessDenied() {
        return "/403";
    }
    
    @RequestMapping("/404")
    public String pageNotFound() {
        return "/404";
    }
}
