package lion.web;

import lion.model.CustomUser;
import lion.service.LionService;
import lion.util.SendMail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author lazar
 */
@Controller
public class PasswordRecoveryController {
    @Qualifier("customUserRepository")
    private final LionService lionService;

    @Autowired
    public PasswordRecoveryController(LionService lionService) {
        this.lionService = lionService;
    }
    
    @RequestMapping(value="/passwordRecovery", method=RequestMethod.GET)
    public String passRecovery() {
        return "passwordRecovery";
    }
    
    @RequestMapping(value="/passwordRecovery", method=RequestMethod.POST)
    public String passRecoverySubmit(@ModelAttribute("email") String email, BindingResult result, Model model) {
        
        CustomUser user = lionService.getCustomUserByEmail(email);
        if(user == null) {
            result.rejectValue("email", "no.such.user.with.email");
        }
        
        String newPass = changePassword(user);
        lionService.updateCustomUser(user);
        boolean sentPassword = SendMail.sendEmail(email, "Password recovery", "Your new password is the following - > " + newPass);
        
        model.addAttribute("sendMessage", sentPassword);
        return "passwordRecovery";
    }
        
    private String changePassword(CustomUser user) {
        BCryptPasswordEncoder passEncoder = new BCryptPasswordEncoder();
        String password = passEncoder.encode(Long.toString(System.currentTimeMillis())).substring(52);
        user.setPass(passEncoder.encode(password));
        return password;
    }
}
