/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lion.web;

import org.springframework.stereotype.Controller;  
import org.springframework.ui.Model;  
import org.springframework.web.bind.annotation.RequestMapping;  
  
import java.util.ArrayList;  
import java.util.List;  
  
@Controller  
public class PeopleController  
{  
    private static final List<Person> people = new ArrayList<>();  
  
    static  
    {  
        people.add(new Person(1, "pas"));  
        people.add(new Person(2, "lucia"));  
        people.add(new Person(3, "lucas"));  
        people.add(new Person(4, "siena"));  
    }  
  
    @RequestMapping("/showallpeople")  
    public String showallpeople(Model model)  
    {  
        model.addAttribute("people", people);  
        return "people";  
    }
    
    public static class Person {
        int i = 0;
        String n = "";
        public Person(int i, String n) {
            this.i = i;
            this.n = n;
        }
    }
}