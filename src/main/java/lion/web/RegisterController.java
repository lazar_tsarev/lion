package lion.web;

import javax.validation.Valid;
import lion.model.CustomUser;
import lion.model.CustomUser.UserType;
import lion.model.Host;
import lion.model.Intern;
import lion.service.LionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author lazar
 */
@Controller
public class RegisterController {
    @Qualifier("customUserRepository")
    private final LionService lionService;

    @Autowired
    public RegisterController(LionService customUserService) {
        this.lionService = customUserService;
    }
    
    @Autowired
    Validator validator;
    
    @RequestMapping(value = "/registerHost", method = RequestMethod.GET)
    public String registerHost(Model model) {
        CustomUser user = new CustomUser();
        Host host = new Host();
        host.setUser(user);
        model.addAttribute("host", host);
        return "registerHost";
    }
    
    @RequestMapping(value = "/registerHost", method=RequestMethod.POST)
    public String submitRegisterHost(@Valid Host host, BindingResult result, RedirectAttributes attrs) {
        CustomUser user = host.getUser();
        user.setUserType(UserType.HOST);
        validate(result, user);
        
        if(!result.hasErrors()) {
            lionService.createHost(host);
            attrs.addFlashAttribute("program", "host");
            return "redirect:/successful-registration";
        }

        return "registerHost";
    }
    
    @RequestMapping(value = "/registerIntern", method = RequestMethod.GET)
    public String registerIntern(Model model) {
        CustomUser user = new CustomUser();
        Intern intern = new Intern();
        intern.setUser(user);
        model.addAttribute("intern", intern);
        return "registerIntern";
    }
    
    @RequestMapping(value = "/registerIntern", method=RequestMethod.POST)
    public String submitRegisterIntern(@Valid Intern intern, BindingResult result, RedirectAttributes attrs) {
        CustomUser user = intern.getUser();
        user.setUserType(UserType.INTERN);
        validate(result, user);
        
        if(!result.hasErrors()) {
            lionService.createIntern(intern);
            attrs.addFlashAttribute("program", "intern");
            return "redirect:/successful-registration";
        }

        return "registerIntern";
    }
    
    @RequestMapping(value = "/registerAiesec", method = RequestMethod.GET)
    public String registerAiesec(Model model) {
        model.addAttribute("user", new CustomUser());
        return "registerAiesec";
    }
    
    @RequestMapping(value = "/registerAiesec", method=RequestMethod.POST)
    public String submitRegisterAiesec(CustomUser user, BindingResult result, RedirectAttributes attrs) {
        user.setUserType(UserType.AIESEC);
        validator.validate(user, result);
        String email = user.getEmail();
        if(hasUserWithEmail(email)) {
            result.rejectValue("email", "email.is.used");
        }
        
        if(!result.hasErrors()) {
            lionService.createCustomUser(user);
            attrs.addFlashAttribute("program", "member");
            return "redirect:/successful-registration";
        }

        return "registerAiesec";
    }
    
    private void validate(BindingResult result, CustomUser user) {
        result.setNestedPath("user.");
        validator.validate(user, result);
        result.setNestedPath("");
        
        String email = user.getEmail();
        if(hasUserWithEmail(email)) {
            result.rejectValue("user.email", "email.is.used");
        }
    }
    
    private boolean hasUserWithEmail(String email) {
        CustomUser user = lionService.getCustomUserByEmail(email);
        return user != null;
    }
    
    @RequestMapping(value = "/successful-registration", method = RequestMethod.GET)
    public String registerSuccess(@ModelAttribute("program") String program, Model model) {
        if(program == null || program.isEmpty()) {
            return "redirect:/login";
        }
        else {
            return "successful-registration";
        }
    }
}
