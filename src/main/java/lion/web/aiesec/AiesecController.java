package lion.web.aiesec;

import lion.model.CustomUser;
import lion.model.Note;
import lion.service.LionService;
import lion.util.UserUtil;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author lazar
 */
@Controller
public class AiesecController {
    
    // TODO : show uploaded pictures in edit form for host.
    
    @Qualifier("customUserRepository")
    private final LionService lionService;
    
    @Autowired
    public AiesecController(LionService lionService) {
        this.lionService = lionService;
    }
    
    @Autowired
    Validator validator;
    
    @RequestMapping(value={"aiesec/problems", "aiesec/problems/"}, method=RequestMethod.GET)
    public String viewNotes(Model model) {
        model.addAttribute("problems", lionService.getProblems());
        return "aiesec/problems";
    }
    
    @RequestMapping(value="aiesec/view-notes/{aboutUserId}", method=RequestMethod.GET)
    public String viewNotes(@PathVariable int aboutUserId, Model model) {
        CustomUser aboutUser = lionService.getCustomUserByID(aboutUserId);
        if(null == aboutUser) {
            return "redirect:/user/error-message-page";
        }
        model.addAttribute("aboutUser", aboutUser);
        model.addAttribute("notes", lionService.getNotesAboutUser(aboutUserId));
        return "aiesec/view-notes";
    }
    
    @RequestMapping(value="aiesec/add-note/{aboutUserId}", method=RequestMethod.GET)
    public String addNote(@PathVariable int aboutUserId, Model model) {
        CustomUser aboutUser = lionService.getCustomUserByID(aboutUserId);
        if(null == aboutUser) {
            return "redirect:/user/error-message-page";
        }
        Note newNote = new Note();
        newNote.setAbout(aboutUser);
        model.addAttribute("aboutUser", aboutUser);
        model.addAttribute("newNote", newNote);
        return "aiesec/add-note";
    }
    
    @RequestMapping(value="aiesec/add-note/", method=RequestMethod.POST)
    public String addNoteSubmit(@ModelAttribute("newNote") Note newNote, int aboutId, BindingResult result, Model model) {
        CustomUser user = UserUtil.getCustomUserFromSession(lionService);
        CustomUser aboutUser = lionService.getCustomUserByID(aboutId);
        
        if(null == aboutUser) {
            return "redirect:/user/error-message-page";
        }
        
        newNote.setAbout(aboutUser);
        newNote.setAuthor(user);
        newNote.setDateCreated(new DateTime());
        
        validator.validate(newNote, result);
        
        if(!result.hasErrors()) {
            lionService.createNewNote(newNote);
            model.addAttribute("success", "Successsfully added new note!");
            return "user/success-page";
        }
        
        System.out.println(result.getAllErrors().toString());
        return "aiesec/add-note";
    }
}
