package lion.web.aiesec;

import java.util.List;
import lion.model.CustomUser;
import lion.model.Story;
import lion.service.LionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author lazar
 */
@Controller
public class ApprovingController {
    
    @Qualifier("customUserRepository")
    private final LionService lionService;
    
    @Autowired
    public ApprovingController(LionService lionService) {
        this.lionService = lionService;
    }
    
    @RequestMapping(value="aiesec/approve-new-users", method=RequestMethod.GET)
    public String approveNewUsers(Model model) {
        List<CustomUser> newUsers = lionService.getNewUsers();
        model.addAttribute("newUsers", newUsers);
        return "aiesec/approve-new-users";
    }
    
    @RequestMapping(value="aiesec/activate-profile/{profileId}", method=RequestMethod.GET)
    public String activateProfile(@PathVariable("profileId") int profileId, Model model) {
        
        CustomUser userToActivate = lionService.getCustomUserByID(profileId);
        if(null == userToActivate || userToActivate.isActivated()) {
            return "redirect:/user/error-message-page";
        }
        userToActivate.setActivated(true);
        lionService.updateCustomUser(userToActivate);
        return "redirect:/user/view-profile/" + profileId;
    }
    
    // TODO : needs work here !!!!!!!
    @RequestMapping(value="aiesec/approve-new-stories", method=RequestMethod.GET)
    public String approveNewStories(Model model) {
        model.addAttribute("newStories", lionService.getNewStories());
        return "aiesec/approve-new-stories";
    }
    
    @RequestMapping(value="aiesec/approve-story/{storyId}", method=RequestMethod.GET)
    public String approveStory(@PathVariable("storyId") int storyId, Model model) {        
        Story storyToApprove = lionService.getStory(storyId);
        if(null == storyToApprove || storyToApprove.getApproved()) {
            return "redirect:/aiesec/error-message-page";
        }
        storyToApprove.setApproved(true);
        lionService.updateStory(storyToApprove);
        // TODO : create view-story !!!
        return "redirect:/user/view-story/" + storyId;
    }
}
