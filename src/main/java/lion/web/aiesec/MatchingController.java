package lion.web.aiesec;

import java.util.Arrays;
import java.util.List;
import lion.model.CustomUser;
import lion.model.CustomUser.UserType;
import lion.model.Matching;
import lion.service.LionService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author lazar
 */
@Controller
public class MatchingController {
    
    @Qualifier("customUserRepository")
    private final LionService lionService;

    @Autowired
    public MatchingController(LionService lionService) {
        this.lionService = lionService;
    }
    
    @RequestMapping(value="/aiesec/newMatching", method=RequestMethod.GET)
    public String newMatching(Model model) {
        loadHostsInternsBuddies(model);
        model.addAttribute("host", 0);
        model.addAttribute("intern", 0);
        model.addAttribute("buddy", 0);
        model.addAttribute("startDate", "");
        model.addAttribute("endDate", "");
        return "aiesec/newMatching";
    }
    
    @RequestMapping(value="/aiesec/newMatching", method=RequestMethod.POST)
    public String newMatchingSubmit(@ModelAttribute("host") String host, @ModelAttribute("intern") String intern, @ModelAttribute("buddy") String buddy, 
                                    @ModelAttribute("startDate") String startDate, @ModelAttribute("endDate") String endDate, Model model) {
        loadHostsInternsBuddies(model);
        boolean isOk = true;
        CustomUser hostUser = parseUser(UserType.HOST, host, model, true);
        CustomUser internUser = parseUser(UserType.INTERN, intern, model, true);
        CustomUser buddyUser = parseUser(UserType.AIESEC, buddy, model, false);
        if(hostUser == null || internUser == null) {
            isOk = false;
        }
        
        DateTime start = null, end = null;
        try {
            start = parseDateTime(startDate);
        } catch(IllegalArgumentException ex) {
            isOk = false;
            model.addAttribute("invalidStartDate", true);
        }
        
        try {
            end = parseDateTime(endDate);
        } catch(IllegalArgumentException ex) {
            isOk = false;
            model.addAttribute("invalidEndDate", true);
        }
        
        if(isOk) {
            Matching newMatching = new Matching();
            newMatching.setHost(hostUser);
            newMatching.setIntern(internUser);
            newMatching.setBuddy(buddyUser);
            newMatching.setStartDate(start);
            newMatching.setEndDate(end);
            
            lionService.createNewMatching(newMatching);
            model.addAttribute("success", "Successsfully created new matching!");
            return "user/success-page";
        }
        
        return "aiesec/newMatching";
    }
    
    private CustomUser parseUser(UserType type, String stringId, Model model, boolean markError) {
        CustomUser user = null;
        Integer userId = null;
        try {
            userId = Integer.parseInt(stringId);
        } catch (NumberFormatException ex) {
            if(markError) {
                model.addAttribute("invalid" + type, true);
            }
        }
        if(null != userId) {
            user = lionService.getCustomUserByID(userId);
            if(user != null) {
                if(!user.getUserType().equals(type) && markError) {
                    model.addAttribute("invalid" + type, true);
                }
            }
            if(user == null && markError) {
                model.addAttribute("invalid" + type, true);
            }
        }
        
        return user;
    }
    
    private void loadHostsInternsBuddies(Model model) {
        List<CustomUser> hosts = lionService.getCustomUsersWithType(UserType.HOST),
                       interns = lionService.getCustomUsersWithType(UserType.INTERN),
                       aiesecs = lionService.getCustomUsersWithType(UserType.AIESEC);
        
        model.addAttribute("hosts", hosts);
        model.addAttribute("interns", interns);
        model.addAttribute("aiesecs", aiesecs);
    }
    
    private DateTime parseDateTime(String date) {
        String[] dateArr = date.split("/");
        if(dateArr.length != 3) {
            throw new IllegalArgumentException("invalid date");
        }
        return new DateTime(Integer.parseInt(dateArr[0]),
                            Integer.parseInt(dateArr[1]),
                            Integer.parseInt(dateArr[2]),
                            0, 0);
    }
}
