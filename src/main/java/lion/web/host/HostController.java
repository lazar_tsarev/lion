package lion.web.host;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import lion.model.CustomUser;
import lion.model.Feedback;
import lion.model.Host;
import lion.model.Matching;
import lion.model.Picture;
import lion.service.LionService;
import lion.util.FileUploadForm;
import lion.util.FileUploadUtil;
import lion.util.UserUtil;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author lazar
 */
@Controller
public class HostController {
    @Qualifier("customUserRepository")
    private final LionService lionService;

    @Autowired
    public HostController(LionService lionService) {
        this.lionService = lionService;
    }
    
    @Autowired
    Validator validator;
    
    @Autowired
    ServletContext servletContext;
    
    @RequestMapping(value="/host/matched-interns", method=RequestMethod.GET)
    public String matchedInterns(Model model) {
        CustomUser user = UserUtil.getCustomUserFromSession(lionService);
        
        List<Matching> matchings = lionService.getMatchingsWithUniqueIntern(user.getId());
        model.addAttribute("matchings", matchings);
        return "host/matched-interns";
    }
    
    @RequestMapping(value="/host/newfeedback/{aboutInternId}", method=RequestMethod.GET)
    public String newFeedback(@PathVariable("aboutInternId") int aboutInternId, Model model) {
        Feedback newFeedback = new Feedback();
        newFeedback.setAbout(lionService.getCustomUserByID(aboutInternId));
        model.addAttribute("newFeedback", newFeedback);
        return "host/newfeedback";
    }
    
    @RequestMapping(value={"/host/newfeedback/", "/host/newfeedback"}, method=RequestMethod.POST)
    public String newFeedbackSubmit(@ModelAttribute("newFeedback") Feedback newFeedback, 
                                   int receiverId, BindingResult result, Model model) {
        
        CustomUser sender = UserUtil.getCustomUserFromSession(lionService);
        CustomUser receiver = lionService.getCustomUserByID(receiverId);
        
        if(receiver == null || receiver.getId().equals(sender.getId()) || !receiver.getUserType().equals(CustomUser.UserType.INTERN)) {
            return "redirect:/host/error-feedback-page";
        }
        
        newFeedback.setAbout(receiver);
        newFeedback.setAuthor(sender);
        newFeedback.setDateCreated(new DateTime());
        newFeedback.setApproved(false);
        validator.validate(newFeedback, result);

        if(!result.hasErrors()) {
            lionService.createNewFeedback(newFeedback);
            return "redirect:/host/matched-interns";
        }
        return "host/newfeedback";
    }
    
    @RequestMapping(value="/host/error-feedback-page")
    public String errorMessagePage() {
        return "host/error-feedback-page";
    }
    
    @RequestMapping(value={"/host/edit", "/host/edit/"}, method=RequestMethod.GET)
    public String edit(Model model) {
        CustomUser user = UserUtil.getCustomUserFromSession(lionService);
        Host host = lionService.getHostForCustomUser(user.getId());
        model.addAttribute("host", host);
        model.addAttribute("fileUploadForm", new FileUploadForm());
        return "host/edit";
    }
    
    // TODO : may implement functionality for deleting pictures and not on every update to delete all old pictures
    @RequestMapping(value="/host/edit", method=RequestMethod.POST)
    public String editSubmit(@ModelAttribute Host host, @ModelAttribute FileUploadForm fileUploadForm, 
                            BindingResult result, Model model) {
        boolean isOk = true;
        CustomUser user = UserUtil.getCustomUserFromSession(lionService);
        Host origHost = lionService.getHostForCustomUser(user.getId());

        host.setId(origHost.getId());
        host.setUser(user);

        validator.validate(host, result);
        
        int i = 0;
        for(MultipartFile file : fileUploadForm.getFiles()) {
            if(!file.isEmpty()) {
                String contentType = file.getContentType();
                if (!contentType.equals("image/jpeg") && !contentType.equals("image/png")) {
                    isOk = false;
                    model.addAttribute("invalidFile" + i, true);
                }

                if(file.getSize() > 2_000_000) {
                    isOk = false;
                    model.addAttribute("invalidFile" + i, true);
                }
            }
            i++;
        }

        if(!result.hasErrors() && isOk) {
            List<Picture> pictures = new ArrayList<>();
            try {
                for (MultipartFile file : fileUploadForm.getFiles()) {
                    if (!file.isEmpty()) {
                        pictures.add(new Picture(FileUploadUtil.saveImage(file, servletContext), file.getOriginalFilename()));
                    }
                }
                host.setPictures(pictures);
                lionService.updateHost(host);
                
                deletePictures(origHost.getPictures());
                
                model.addAttribute("success", "Successsfully changed your profile information!");
                return "user/success-page";
            } catch (IOException ex) {
                isOk = false;
            }
        }
        
        return "host/edit";
    }
    
    private void deletePictures(List<Picture> pictures) {
        if(pictures != null) {
            for (Picture picture : pictures) {
                try {
                    File file = new File(servletContext.getRealPath("/") + picture.getLocation());
                    if (file.delete()) {
                        lionService.deletePicture(picture);
                    } else {
                        System.out.println("Delete operation has failed.");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
