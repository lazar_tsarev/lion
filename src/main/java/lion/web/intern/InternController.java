package lion.web.intern;

import java.util.List;
import lion.model.CustomUser;
import lion.model.CustomUser.UserType;
import lion.model.Feedback;
import lion.model.Intern;
import lion.model.Matching;
import lion.service.LionService;
import lion.util.UserUtil;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author lazar
 */
@Controller
public class InternController {
    
    @Qualifier("customUserRepository")
    private final LionService lionService;

    @Autowired
    public InternController(LionService lionService) {
        this.lionService = lionService;
    }
    
    @Autowired
    Validator validator;
    
    @RequestMapping(value="/intern/edit", method=RequestMethod.GET)
    public String edit(Model model) {
        CustomUser user = UserUtil.getCustomUserFromSession(lionService);
        Intern intern = lionService.getInternForCustomUser(user.getId());
        model.addAttribute("intern", intern);
        return "intern/edit";
    }
    
    @RequestMapping(value="/intern/edit", method=RequestMethod.POST)
    public String editSubmit(Intern intern, BindingResult result, Model model) {
        CustomUser user = UserUtil.getCustomUserFromSession(lionService);
        Intern origIntern = lionService.getInternForCustomUser(user.getId());
        
        intern.setUser(user);
        intern.setId(origIntern.getId());
        
        validator.validate(intern, result);
        
        if(!result.hasErrors()) {
            lionService.updateIntern(intern);
            model.addAttribute("success", "Successsfully changed your profile information!");
            return "user/success-page";
        }
        return "intern/edit";
    }
    
    @RequestMapping(value="/intern/matched-hosts", method=RequestMethod.GET)
    public String matchedHosts(Model model) {
        CustomUser user = UserUtil.getCustomUserFromSession(lionService);
        
        List<Matching> matchings = lionService.getMatchingsWithUniqueHost(user.getId());
        model.addAttribute("matchings", matchings);
        return "intern/matched-hosts";
    }
    
    @RequestMapping(value="/intern/newfeedback/{aboutHostId}", method=RequestMethod.GET)
    public String newFeedback(@PathVariable("aboutHostId") int aboutHostId, Model model) {
        Feedback newFeedback = new Feedback();
        newFeedback.setAbout(lionService.getCustomUserByID(aboutHostId));
        model.addAttribute("newFeedback", newFeedback);
        return "intern/newfeedback";
    }
    
    @RequestMapping(value={"/intern/newfeedback/", "/intern/newfeedback"}, method=RequestMethod.POST)
    public String newFeedbackSubmit(@ModelAttribute("newFeedback") Feedback newFeedback, 
                                   int receiverId, BindingResult result, Model model) {
        
        CustomUser sender = UserUtil.getCustomUserFromSession(lionService);
        CustomUser receiver = lionService.getCustomUserByID(receiverId);
        
        if(receiver == null || receiver.getId().equals(sender.getId()) || !receiver.getUserType().equals(UserType.HOST)) {
            return "redirect:/intern/error-feedback-page";
        }
        
        newFeedback.setAbout(receiver);
        newFeedback.setAuthor(sender);
        newFeedback.setDateCreated(new DateTime());
        newFeedback.setApproved(false);
        validator.validate(newFeedback, result);

        if(!result.hasErrors()) {
            lionService.createNewFeedback(newFeedback);
            return "redirect:/intern/matched-hosts";
        }
        return "intern/newfeedback";
    }
    
    @RequestMapping(value="/intern/error-feedback-page")
    public String errorFeedbackPage() {
        return "intern/error-feedback-page";
    }
}
