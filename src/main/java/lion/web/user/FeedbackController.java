package lion.web.user;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import lion.model.CustomUser;
import lion.model.Picture;
import lion.model.Problem;
import lion.model.Story;
import lion.service.LionService;
import lion.util.FileUploadForm;
import lion.util.FileUploadUtil;
import lion.util.UserUtil;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author lazar
 */
@Controller
public class FeedbackController {
    private static final String youtubeRegex = "^(?:https?:\\/\\/)?(?:www\\.)?(?:youtu\\.be\\/|youtube\\.com\\/(?:embed\\/|v\\/|watch\\?v=|watch\\?.+&v=))((\\w|-){11})(?:\\S+)?$";
    
    @Qualifier("customUserRepository")
    private final LionService lionService;

    @Autowired
    public FeedbackController(LionService lionService) {
        this.lionService = lionService;
    }
    
    @Autowired
    Validator validator;
    
    @Autowired
    ServletContext servletContext;
    
    @RequestMapping(value="/user/problem", method=RequestMethod.GET)
    public String problem(Model model) {
        model.addAttribute("usersMap", getOtherUsersMap());
        return "user/problem";
    }
    
    @RequestMapping(value="/user/problem", method=RequestMethod.POST)
    public String problemSubmit(String about, String description, Model model) {
        try {
            boolean isOk = true;
            int aboutId = Integer.parseInt(about);
            CustomUser aboutUser = lionService.getCustomUserByID(aboutId);
            
            if(aboutUser == null) {
                isOk = false;
                model.addAttribute("invalidAbout", true);
            }
            
            if(description.length() < 15 || description.length() > 5000) {
                isOk = false;
                model.addAttribute("descriptionSize", true);
            }
            
            if(isOk) {
                Problem problem = new Problem();
                problem.setAbout(aboutUser);
                problem.setAuthor(UserUtil.getCustomUserFromSession(lionService));
                problem.setDescription(description);
                lionService.createProblem(problem);
                model.addAttribute("success", "Successsfully submitted problem! We will contact with you soon.");
                return "user/success-page";
            }
        } catch (NumberFormatException ex) {
            model.addAttribute("invalidAbout", true);
        }
        
        model.addAttribute("usersMap", getOtherUsersMap());
        return "user/problem";
    }
    
    @RequestMapping(value="/user/story", method=RequestMethod.GET)
    public String story(Model model) {
        model.addAttribute("usersMap", getOtherUsersMap());
        model.addAttribute("fileUploadForm", new FileUploadForm());
        model.addAttribute("about", "0");
        return "user/story";
    }
    
    @RequestMapping(value="/user/story", method=RequestMethod.POST)
    public String storySubmit( String about, @ModelAttribute("endedDate") String endedDate, 
                                @ModelAttribute("startedDate") String startedDate, @ModelAttribute("description") String description, 
                                @ModelAttribute("videoLink") String videoLink, @ModelAttribute("fileUploadForm") FileUploadForm fileUploadForm, 
                                Model model) {
        boolean isOk = true;

        try {
            DateTime started = null, 
                     ended = null;
            int aboutId = Integer.parseInt(about);
            CustomUser aboutUser = lionService.getCustomUserByID(aboutId);
            
            if(aboutUser == null) {
                isOk = false;
                model.addAttribute("invalidAbout", true);
            }
            
            if(description.length() < 15 || description.length() > 5000) {
                isOk = false;
                model.addAttribute("invalidDescription", true);
            }
            
            if(!endedDate.isEmpty()) {
                try {
                    ended = parseDateTime(endedDate);
                } catch(IllegalArgumentException ex) {
                    isOk = false;
                    model.addAttribute("invalidEndedDate", true);
                }
            }
            
            if(!startedDate.isEmpty()) {
                try {
                    started = parseDateTime(startedDate);
                } catch(IllegalArgumentException ex) {
                    isOk = false;
                    model.addAttribute("invalidStartedDate", true);
                }
            }
            
            if(!videoLink.isEmpty() && !videoLink.matches(youtubeRegex)) {
                isOk = false;
                model.addAttribute("invalidVideoLink", true);
            }
            
            int i = 0;
            for(MultipartFile file : fileUploadForm.getFiles()) {
                if(!file.isEmpty()) {
                    String contentType = file.getContentType();
                    if (!contentType.equals("image/jpeg") && !contentType.equals("image/png")) {
                        isOk = false;
                        model.addAttribute("invalidFile" + i, true);
                    }

                    if(file.getSize() > 2_000_000) {
                        isOk = false;
                        model.addAttribute("invalidFile" + i, true);
                    }
                }
                i++;
            }
            
            if(isOk) {
                List<Picture> pictures = new ArrayList<>();
                try {
                    for(MultipartFile file : fileUploadForm.getFiles()) {
                        if(!file.isEmpty()) {
                            pictures.add(new Picture(FileUploadUtil.saveImage(file, servletContext), file.getOriginalFilename()));
                        }
                    }
                    
                    Story story = new Story();
                    story.setAbout(aboutUser);
                    story.setAuthor(UserUtil.getCustomUserFromSession(lionService));
                    story.setDescription(description);
                    story.setStartedDate(started);
                    story.setEndedDate(ended);
                    story.setVideoLink(videoLink);
                    story.setPictures(pictures);
                    story.setApproved(false);
                    
                    lionService.createStory(story);
                    
                    model.addAttribute("success", "Successsfully submitted story!");
                    return "user/success-page";
                    
                } catch (IOException ex) {
                    isOk = false;
                }
            }
            
        } catch(NumberFormatException e) {
            isOk = false;
            model.addAttribute("invalidAbout", true);
        }
        
        model.addAttribute("about", about);
        
        model.addAttribute("usersMap", getOtherUsersMap());
        return "user/story";
    }
    
    private Map<String, String> getOtherUsersMap() {
        List<CustomUser> otherUsers = lionService.getAllUsersWithout(UserUtil.getCustomUserFromSession(lionService).getId());
        Map<String, String> usersMap = new HashMap<>();
        for(CustomUser user : otherUsers) {
            usersMap.put(user.getId().toString(), user.getName() + " " + user.getLastName());
        }
        return usersMap;
    }
    
    private DateTime parseDateTime(String date) {
        String[] dateArr = date.split("/");
        if(dateArr.length != 3) {
            throw new IllegalArgumentException("invalid date");
        }
        return new DateTime(Integer.parseInt(dateArr[0]),
                            Integer.parseInt(dateArr[1]),
                            Integer.parseInt(dateArr[2]),
                            0, 0);
    }
}
