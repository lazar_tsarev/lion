package lion.web.user;

import java.util.List;
import lion.model.CustomUser;
import lion.model.Message;
import lion.service.LionService;
import lion.util.UserUtil;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author lazar
 */
@Controller
public class MessagesController {
    
    @Qualifier("customUserRepository")
    private final LionService lionService;
    
    @Autowired
    public MessagesController(LionService lionService) {
        this.lionService = lionService;
    }
    
    @Autowired
    Validator validator;
    
    @RequestMapping(value="/user/messages/sent", method=RequestMethod.GET)
    public String sentMessages(Model model) {
        List<Message> conversations = lionService.getSentConversationsForUser(UserUtil.getCustomUserFromSession(lionService).getId());
        model.addAttribute("conversations", conversations);
        return "user/sentConversations";
    }
    
    @RequestMapping(value={"/user/messages/inbox", "/user/messages/", "/user/messages"}, method=RequestMethod.GET)
    public String inboxMessages(Model model) {
        List<Message> conversations = lionService.getConversationsForUser(UserUtil.getCustomUserFromSession(lionService).getId());
        model.addAttribute("conversations", conversations);
        return "user/conversations";
    }
    
    @RequestMapping(value="/user/messages/{otherUserId}", method=RequestMethod.GET)
    public String conversation(@PathVariable("otherUserId") int otherUserId, Model model) {
        CustomUser user = UserUtil.getCustomUserFromSession(lionService);
        model.addAttribute("messages", lionService.getMessagesForUsers(user.getId(), otherUserId));
        model.addAttribute("otherUser", lionService.getCustomUserByID(otherUserId));
        return "user/conversation";
    }
    
    @RequestMapping(value="/user/newmessage/{receiverId}", method=RequestMethod.GET)
    public String newMessage(@PathVariable("receiverId") int receiverId, Model model) {
        Message newMessage = new Message();
        newMessage.setReceiver(lionService.getCustomUserByID(receiverId));
        model.addAttribute("newMessage", newMessage);
        return "user/newMessage";
    }
    
    @RequestMapping(value={"/user/newmessage/", "/user/newmessage"}, method=RequestMethod.POST)
    public String newMessageSubmit(@ModelAttribute("newMessage") Message newMessage, 
                                   int receiverId, BindingResult result, Model model) {
        CustomUser sender = UserUtil.getCustomUserFromSession(lionService);
        CustomUser receiver = lionService.getCustomUserByID(receiverId);
        
        if(receiver == null || receiver.getId().equals(sender.getId())) {
            return "redirect:/user/error-message-page";
        }
        
        newMessage.setSender(sender);
        newMessage.setReceiver(receiver);
        newMessage.setDateCreated(new DateTime());
        newMessage.setReaded(false);
        validator.validate(newMessage, result);

        if(!result.hasErrors()) {
            lionService.createNewMessage(newMessage);
            return "redirect:/user/messages/" + newMessage.getReceiver().getId();
        }
        return "user/newMessage";
    }
}
