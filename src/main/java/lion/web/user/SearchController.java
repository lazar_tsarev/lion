package lion.web.user;

import java.util.ArrayList;
import lion.model.CustomUser;
import lion.service.LionService;
import lion.util.SearchForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author lazar
 */
@Controller
public class SearchController {
    
    @Qualifier("customUserRepository")
    private final LionService lionService;
    
    @Autowired
    public SearchController(LionService lionService) {
        this.lionService = lionService;
    }

    @RequestMapping(value={"/user/search", "/user/search/"}, method=RequestMethod.GET)
    public String search(Model model) {
        model.addAttribute("searchForm", new SearchForm());
        model.addAttribute("filteredUsers", new ArrayList<CustomUser>());
        return "user/search";
    }
    
    @RequestMapping(value={"/user/search", "/user/search/"}, method=RequestMethod.POST)
    public String searchSubmit(@ModelAttribute("searchForm") SearchForm searchForm, Model model) {
        System.out.println("\n\n-------------" + searchForm.getName());
        model.addAttribute("filteredUsers", lionService.filterUsers(searchForm));
        model.addAttribute("searchForm", searchForm);
        return "user/search";
    }
}
