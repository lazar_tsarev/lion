package lion.web.user;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import lion.model.CustomUser;
import lion.model.CustomUser.UserType;
import lion.model.Host;
import lion.model.Intern;
import lion.model.Picture;
import lion.service.LionService;
import lion.util.FileUploadUtil;
import lion.util.UserUtil;
import org.imgscalr.Scalr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author lazar
 */
@Controller
public class UserController {
    
    // TODO : move add feedback to the UserController and /user/ folder in the views
    
    @Qualifier("customUserRepository")
    private final LionService lionService;

    @Autowired
    public UserController(LionService lionService) {
        this.lionService = lionService;
    }
    
    @Autowired
    Validator validator;
    
    @Autowired
    ServletContext servletContext;
    
    @RequestMapping(value={"/user/welcome", "/user/"})
    public String adminIndex(Model model) {
        CustomUser user = UserUtil.getCustomUserFromSession(lionService);
        model.addAttribute("user", user);
        return "user/welcome";
    }
    
    // TODO : show full profile attribute in model. Possible scenarios for access to full account :
    //  1. SignedIn user is matched with this user.
    //  2. SignedIn user is AIESEC member
    //  3. there is no third option, I think ?
    @RequestMapping(value="/user/view-profile/{profileId}", method=RequestMethod.GET)
    public String viewProfile(@PathVariable("profileId") int profileId, Model model) {
        CustomUser user = UserUtil.getCustomUserFromSession(lionService);
        CustomUser viewUser = lionService.getCustomUserByID(profileId);

        if(null == viewUser || (!user.getUserType().equals(UserType.AIESEC) && !viewUser.isActivated())) {
            return "redirect:/user/error-message-page";
        }
        
        model.addAttribute("user", viewUser);
        model.addAttribute("signedInUser", user.getId().equals(viewUser.getId()));
        model.addAttribute("viewFullProfile", hasAccessToFullProfile(user, viewUser));
        injectFullProfile(viewUser, model);
        return "user/view-profile";
    }
    
    private boolean hasAccessToFullProfile(CustomUser user, CustomUser viewUser) {
        return  user.getUserType().equals(UserType.AIESEC) || 
                lionService.hasMatchingWithUsers(user.getId(), viewUser.getId());
    }
    
    private void injectFullProfile(CustomUser user, Model model) {
        switch(user.getUserType()) {
            case INTERN:
                Intern intern = lionService.getInternForCustomUser(user.getId());
                model.addAttribute("intern", intern);
                System.out.println("\n\ninserted intern " + intern + "\n\n");
                break;
            case HOST:
                Host host = lionService.getHostForCustomUser(user.getId());
                model.addAttribute("host", host);
                System.out.println("\n\ninserted host " + host + "\n\n");
                break;
        }
    }
    
    @RequestMapping(value={"/user/view-profile", "/user/view-profile/"}, method=RequestMethod.GET)
    public String viewSignedProfile(Model model) {
        model.addAttribute("user", UserUtil.getCustomUserFromSession(lionService));
        model.addAttribute("signedInUser", true);
        model.addAttribute("viewFullProfile", true);
        return "user/view-profile";
    }
    
    @RequestMapping(value="/user/edit", method=RequestMethod.GET)
    public String edit(Model model) {
        CustomUser user = UserUtil.getCustomUserFromSession(lionService);
        model.addAttribute("user", user);
        model.addAttribute("result", "");
        return "user/edit";
    }

    @RequestMapping(value="/user/edit", method=RequestMethod.POST)
    public String editSubmit(@ModelAttribute("user") CustomUser editedUser, BindingResult result, Model model) {
        CustomUser user = UserUtil.getCustomUserFromSession(lionService);
        
        editedUser.setId(user.getId());
        editedUser.setEmail(user.getEmail());
        editedUser.setPass(user.getPass());
        editedUser.setActivated(user.isActivated());
        
        editedUser.setUserType(user.getUserType());
        editedUser.setIsStatic(user.isIsStatic());
        editedUser.setQuestions(user.getQuestions());
        
        validator.validate(editedUser, result);
        
        if(result.hasErrors()) {
            model.addAttribute("result", "");
        } else {
            model.addAttribute("result", "Successfully updated information!");
            lionService.updateCustomUser(editedUser);
        }
        
        return "user/edit";
    }
    
    @RequestMapping(value="/user/changePassword", method=RequestMethod.GET)
    public String changePassword(Model model) {
        return "user/changePassword";
    }

    @RequestMapping(value="/user/changePassword", method=RequestMethod.POST)
    public String changePasswordSubmit(@ModelAttribute("oldPassword") String oldPassword,
                                        @ModelAttribute("newPassword") String newPassword,
                                        @ModelAttribute("repeatPassword") String repeatPassword,
                                        Model model) {
        boolean okPass = true;
        CustomUser user = UserUtil.getCustomUserFromSession(lionService);
        if(UserUtil.passMatch(oldPassword, user.getPass())) {
            if(newPassword.length() < 6 || newPassword.length() > 60) {
                okPass = false;
                model.addAttribute("newPasswordErr", true);
            }
            if(!newPassword.equals(repeatPassword)) {
                okPass = false;
                model.addAttribute("repeatPasswordErr", true);
            }
        }
        else {
            okPass = false;
            model.addAttribute("oldPasswordErr", true);
        }
        
        if(okPass) {
            user.setPass(UserUtil.hashPass(newPassword));
            lionService.updateCustomUser(user);
            model.addAttribute("success", "Successsfully changed password !");
            return "user/success-page";
        }
        return "user/changePassword";
    }
    
    @RequestMapping(value="/user/avatar", method=RequestMethod.GET)
    public String avatar(Model model) {
        CustomUser user = UserUtil.getCustomUserFromSession(lionService);
        model.addAttribute("avatar", user.getAvatar());
        return "user/avatar";
    }
    
    @RequestMapping(value="/user/avatar", method=RequestMethod.POST)
    public String avatarSubmit(MultipartFile avatarFile, Model model) {
        boolean hasErr = false;
        Picture avatar = null;
        
        if (!avatarFile.isEmpty()) {
            String contentType = avatarFile.getContentType();
            System.out.println(contentType);
            String type = null;
            switch(contentType) {
                case "image/jpeg":
                    type = "jpg";
                    break;
                case "image/png":
                    type = "png";
                    break;
                default:
                    hasErr = true;
            }
            if (!contentType.equals("image/jpeg")) {
                hasErr = true;
            }
            
            if(avatarFile.getSize() > 2_000_000) {
                hasErr = true;
            }
            
            if(!hasErr) {
                try {
                    BufferedImage bufferedImage = ImageIO.read(avatarFile.getInputStream());
                    bufferedImage = Scalr.resize(bufferedImage, Scalr.Method.SPEED, Scalr.Mode.AUTOMATIC, 100, 100);
                    String relPath = FileUploadUtil.saveImage(System.currentTimeMillis() + "." + type, type, bufferedImage, servletContext);
                    CustomUser user = UserUtil.getCustomUserFromSession(lionService);
                    
                    Picture oldAvatar = user.getAvatar();
                    
                    avatar = new Picture(relPath, "avatar");
                    user.setAvatar(avatar);
                    lionService.updateCustomUser(user);
                    
                    deleteOldAvatar(oldAvatar);
                    model.addAttribute("success", "Successsfully changed avatar!");
                    return "user/success-page";
                } catch (IOException e) {
                    hasErr = true;
                }
            }
        }
        
        model.addAttribute("avatarErr", hasErr);
        model.addAttribute("avatar", avatar);
        return "user/avatar";
    }
    
    @RequestMapping(value="/user/error-messsage-page")
    public String errorMessagePage() {
        return "user/error-message-page";
    }
    
    @RequestMapping(value="/user/add-feedback/{profileId}", method=RequestMethod.GET)
    public String addFeedback(@PathVariable("profileId") int profileId, Model model) {
        CustomUser user = lionService.getCustomUserByID(profileId);
        String type = user.getUserType().toString().toLowerCase();
        if(type.equals("aiesec")) {
            return "redirect:/user/view-profile/" + profileId;
        }
        else {
            return "redirect:/" + type + "/add-feedback/" + profileId;
        }
    }
    
    @RequestMapping(value="/user/view-story/{storyId}")
    public String viewStory(@PathVariable("storyId") int storyId, Model model) {
        model.addAttribute("story", lionService.getStory(storyId));
        return "user/view-story";
    }
    
    private void deleteOldAvatar(Picture avatar) {
        if(avatar != null) {
            try {
                File file = new File(servletContext.getRealPath("/") + avatar.getLocation());
                if (file.delete()) {
                    lionService.deletePicture(avatar);
                } else {
                    System.out.println("Delete operation has failed.");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
