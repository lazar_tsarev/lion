-- users passwords for the users are qwerty -> 123456 -> holymoly
INSERT INTO custom_users(id, email, last_name, name, pass, activated, is_static, birth_date, english, gender, user_type) 
    VALUES  (1, "email@abv.bg", "ail", "em", "$2a$10$HYZCH0iq2WncB3HrrtujzuWs.x0qD.PNZWolMP/zVV5V.7UyeK9pG", true, true, 
                '1992-05-03 00:00:00', "poor", "male", "HOST"),
            (2, "special@abv.bg", "ial", "spec", "$2a$10$/P1sgmIDurtYPgEE4/DOA.R1ernvbOfQIs7aU2SjthmfNjjMJ1KOi", true, true, 
                '1960-06-04 00:00:00', "perfect", "male", "INTERN"),
            (3, "vajnoe@abv.bg", "noe", "vaj", "$2a$10$FruURfRd0f5aRdxamwHi5OsQoE8zsFygB0p/1dsQaeNi18NoSCyl6", true, true, 
                '1997-01-05 00:00:00', "good", "female", "AIESEC");

INSERT INTO custom_users( id, email, last_name, name, pass, activated, is_static, birth_date, english, gender, user_type )
VALUES 
( 4, "godjo@abv.bg", "Goshov", "Petar", "$2a$10$HYZCH0iq2WncB3HrrtujzuWs.x0qD.PNZWolMP/zVV5V.7UyeK9pG", FALSE , TRUE , '1992-05-03 00:00:00', "poor", "male", "HOST" ) , 
( 5, "maniwiq@abv.bg", "Todorov", "Ivan", "$2a$10$/P1sgmIDurtYPgEE4/DOA.R1ernvbOfQIs7aU2SjthmfNjjMJ1KOi", FALSE , TRUE , '1960-06-04 00:00:00', "perfect", "male", "INTERN" ) , 
( 6, "aistigawemais@abv.bg", "Mandjurov", "Georgi", "$2a$10$FruURfRd0f5aRdxamwHi5OsQoE8zsFygB0p/1dsQaeNi18NoSCyl6", FALSE , TRUE , '1997-01-05 00:00:00', "good", "female", "AIESEC" );

INSERT INTO `lion`.`hosts` (`about_couch`, `address`, `max_interns`, `private_bed`, `private_room`, `smoking`, `wheelchair_access`, `user_id`) 
                    VALUES ('', 'aaaaaaaadresa e mnogo gotin', '22', '0', '1', '0', '0', '1'),
                           ('', 'o6te edin mnogo gotin adres', '3', '1', '0', '1', '1', '4');

INSERT INTO `lion`.`interns` (`preferred_gender`, `requirements`, `user_id`) 
                      VALUES ('any', 'do not have any requirements', '2'),
                             ('male', 'he has to be white, max 32 years old and max weight 80 kilos', '5');


--pictures for host with id = 1
INSERT INTO `lion`.`pictures` (`alt`, `location`, `for_host_id`) VALUES 
                              ('my simple alt', '/resources/images/uploaded/man_of_steel.png', '1'),
                              ('my basketball alt', '/resources/images/uploaded/basketball.jpg', '1');

-- matchings
INSERT INTO `lion`.`matchings` (`end_date`, `start_date`, `buddy_id`, `host_id`, `intern_id`) 
                        VALUES ('2014-09-01 00:00:00', '2014-09-10 00:00:00', '3', '1', '2');

-- messages
INSERT INTO `lion`.`messages` (`content`, `title`, `date_created`, `readed`, `receiver_id`, `sender_id`) 
                       VALUES ('my first message', 't1', DATE_SUB(NOW(), INTERVAL 3 HOUR), '0', '1', '2'),
                              ('my second message','t2', DATE_SUB(NOW(), INTERVAL 3 HOUR), '0', '1', '2'),
                              ('my third message', 't3', DATE_SUB(NOW(), INTERVAL 3 HOUR), '0', '2', '1'),

                              ('damn biotch',      't4', DATE_SUB(NOW(), INTERVAL 3 HOUR), '0', '3', '1'),
                              ('my fifth page',    't5', DATE_SUB(NOW(), INTERVAL 3 HOUR), '0', '1', '3'),
                              ('aide be ne vervam','t6', DATE_SUB(NOW(), INTERVAL 3 HOUR), '0', '3', '1'),
                              ('iiii nai-nakraq',  't7', DATE_SUB(NOW(), INTERVAL 3 HOUR), '0', '1', '3');

--stories
INSERT INTO `lion`.`stories` (`approved`,`description`, `ended_date`, `started_date`, `about_id`, `author_id`) VALUES 
('1', 'adsaoqwe description adsaoqwe description adsaoqwe description adsaoqwe description adsaoqwe description adsaoqwe description adsaoqwe description', '2014-09-01 00:00:00', '2014-10-02 00:00:00', '2', '1');